	/*Javascript Encuesta*/

	var selector1 = document.getElementById('selectorOne');
	var selector2 = document.getElementById('selectorTwo');
	var selector3 = document.getElementById('selectorThree');

	var adv1= document.getElementById('Advertencia1');
	var adv2= document.getElementById('Advertencia2');
	var adv3= document.getElementById('Advertencia3');

	selector1.addEventListener("change", function() {	    
		validador(selector1, adv1);  
		validarForm(selector1,selector2,selector3);
	}); 

	selector2.addEventListener("change", function() {	
		validador(selector2, adv2); 
		validarForm(selector1,selector2,selector3);
	}); 

	selector3.addEventListener("change", function() {	
		validador(selector3, adv3);  
		validarForm(selector1,selector2,selector3);  
	}); 

	function validador(selector, advertencia){
		if(selector.value == 'A'){
			console.log('Respuesta Acertada');
			selector.style.border= 'green solid 2px';	
			advertencia.style.display= 'block';
			advertencia.innerHTML = 'Respuesta Acertada';
			advertencia.style.color = 'green';
		}else if(selector.value == ''){
			selector.style.border= 'grey solid 2px';	
			advertencia.style.display= 'none';
			advertencia.style.color = 'grey';
		}
		else{
			console.log('Respuesta Equivocada');
			selector.style.border= 'red solid 2px';
			advertencia.style.display= 'block';
			advertencia.innerHTML = 'Respuesta Incorrecta';
			advertencia.style.color = 'red';
		}
	}

	function validarForm(selec1,selec2,selec3){
		var btn = document.getElementById('btnValidar');
		console.log(btn);
		if ((selec1.value == 'A' && selec2.value == 'A' ) && selec3.value== 'A') {
			btn.disabled= false;
		}else{
			btn.disabled= true;
		}
	}
